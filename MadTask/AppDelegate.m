#import "AppDelegate.h"
#import "ViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    ViewController *vc1 = [[ViewController alloc] init];
    RouterButtonConfig *vc1ButtonConfig = [self buttonConfigWithTitle:@"Push 🌚"];
    vc1.view.backgroundColor = [UIColor yellowColor];
    vc1.buttonConfig = vc1ButtonConfig;
    
    ViewController *vc2 = [[ViewController alloc] init];
    RouterButtonConfig *vc2ButtonConfig = [self buttonConfigWithTitle:@"Pop 🌝"];
    vc2.view.backgroundColor = [UIColor greenColor];
    vc2.buttonConfig = vc2ButtonConfig;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc1];
    
    vc1ButtonConfig->onTapBlock = ^{
        [vc1.navigationController pushViewController:vc2 animated:YES];
    };
    
    vc2ButtonConfig->onTapBlock = ^{
        [vc2.navigationController popViewControllerAnimated:YES];
    };
    
    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (RouterButtonConfig *)buttonConfigWithTitle:(NSString *)title {
    RouterButtonConfig *config = [RouterButtonConfig new];
    
    NSDictionary *attributes = @{
                                NSFontAttributeName: [UIFont systemFontOfSize:40.0],
                                NSForegroundColorAttributeName: [UIColor purpleColor]
                                };
    
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:title
                                                                 attributes:attributes];
    
    config->title = attrString;
    
    return config;
}

@end
