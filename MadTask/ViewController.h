#import <UIKit/UIKit.h>

@interface RouterButtonConfig : NSObject {
@public
    NSAttributedString *title;
    dispatch_block_t onTapBlock;
}

@end

@interface ViewController : UIViewController

@property (nonatomic, assign) RouterButtonConfig *buttonConfig;

@end

