#import "ViewController.h"
#import <mach/mach.h>

@implementation RouterButtonConfig

@end


@interface ViewController()

@property (nonatomic, strong) UIButton *routeButton;
@property (nonatomic, strong) UILabel *infoLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.routeButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [self.routeButton setAttributedTitle:self.buttonConfig->title forState:UIControlStateNormal];
    [self.routeButton addTarget:self action:@selector(onButtonTap) forControlEvents:UIControlEventTouchUpInside];
    [self.routeButton sizeToFit];
    [self.view addSubview:self.routeButton];
    
    self.infoLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.infoLabel.frame = CGRectMake(0.0, self.view.frame.size.height - 100.0, self.view.frame.size.width, 20.0);
    self.infoLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    self.infoLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.view addSubview:self.infoLabel];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Put button to the center of the VC
    self.routeButton.center = CGPointMake(CGRectGetMidX(self.view.frame), CGRectGetMidY(self.view.frame));
    
    [self.navigationItem setHidesBackButton:YES animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self startTicking];
}

- (void)onButtonTap {
    self.buttonConfig->onTapBlock();
}

- (void)startTicking {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        while (self) {
            [self updateInfo];
            usleep(100);
        }
    });
}

- (void)updateInfo {
    NSDateFormatter *formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"HH:mm:ss"];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    NSString *resultString = [NSString stringWithFormat:@"Memory usage is ~ %.01fMb on %@", GetProcessMemoryUsage(), dateString];
    self.infoLabel.text = resultString;
}

float GetProcessMemoryUsage() {
    struct task_basic_info info;
    mach_msg_type_number_t size = sizeof(info);
    kern_return_t kr;
    kr = task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)&info, &size);
    if (kr == KERN_SUCCESS)
        return (float)(info.resident_size) / 1024.0 / 1024.0;
    else
        return 0;
}

@end
